//
//  ViewController.swift
//  UISliderLesson
//
//  Created by Evgeniy Suprun on 15/02/2019.
//  Copyright © 2019 Evgeniy Suprun. All rights reserved.
//

import UIKit
import AVFoundation
// import MediaPlayer

class ViewController: UIViewController {
    
    // load AVplayer
    var player = AVAudioPlayer()
    var isPlaying = false
    var timer = Timer()
    
    
    // Create play buttom
    let playButton = UIButton()
    //Create stop button
    let stopButton = UIButton()
    // Create music name text label
    let musicTextLabel = UITextView()
    // Create timer text label
    let timerTextLabel = UITextView()
    // Create end of song timer
    var timerTextLabelAll = UITextView()
    // Create Slider volume
    let musicSlider = UISlider()
    // Create volume text label
    let volumeTextLabel = UITextView()
    // Create music slider change time
    let timerForSlider = UISlider()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // set Audioplayer parametres
        do {
            if let path = Bundle.main.path(forResource: "Swanky", ofType : "mp3") {
                try player = AVAudioPlayer(contentsOf: URL(fileURLWithPath : path))
            
  
            }
            
        } catch {
            
            print ("Error")
            
        }
        
        // backgroud play music
        do {
            try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default)
            try AVAudioSession.sharedInstance().setActive(true)
        } catch {
            print ("Error")
        }
        
        // Set timers for player options
        
        
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(timeUpdate), userInfo: nil, repeats: true)
        
        
        // create label with music name
        
        self.musicTextLabel.frame = CGRect(x: 115, y: 150, width: 200, height: 50)
        self.musicTextLabel.text = "Swanky - mp3"
        self.musicTextLabel.font = UIFont.boldSystemFont(ofSize: 22)
        self.view.addSubview(musicTextLabel)
        
        // create label with timer music left changes
        
        self.timerTextLabel.frame = CGRect(x: 50, y: 300, width: 50, height: 20)
        self.timerTextLabel.font = UIFont(name: "Avenir", size: 10)
        self.view.addSubview(timerTextLabel)
        
        // create label with timer music right all song
        
        self.timerTextLabelAll.frame = CGRect(x: 300, y: 300, width: 50, height: 20)
        self.timerTextLabelAll.font = UIFont(name: "Avenir", size: 10)
        self.view.addSubview(timerTextLabelAll)
        
        // create music slider change time
        
        let currentTime = Int(player.duration)
        let minutes = currentTime / 60
        let second = currentTime - minutes * 60
        self.timerForSlider.frame = CGRect(x: 90, y: 300, width: 200, height: 30)
        let imageTimer = UIImage(named: "timerslide")
        self.timerForSlider.setThumbImage(imageTimer, for: .normal)
        self.timerForSlider.minimumValue = 0.0
        self.timerForSlider.maximumValue = Float(player.duration)

        self.timerForSlider.addTarget(self, action: #selector(sliderFunc(sender:)), for: .valueChanged)
        self.view.addSubview(timerForSlider)
        
        
        // create music slider volume
        
        self.musicSlider.frame = CGRect(x: 100, y: 400, width: 180, height: 50)
        self.musicSlider.value = 0.5
        
        let ImageVolume = UIImage(named: "volume-like")
        self.musicSlider.setThumbImage(ImageVolume, for: .normal)
        self.view.addSubview(musicSlider)
        
        // create volume procent text label
        
        self.volumeTextLabel.frame = CGRect(x: 130, y: 450, width: 120, height: 50)
        self.volumeTextLabel.font = UIFont(name: "Avenir", size: 17)
        self.view.addSubview(volumeTextLabel)
        
        // create button play parametres
        
        self.playButton.frame = CGRect(x: 70, y: 500, width: 100, height: 50)
        self.playButton.backgroundColor = .black
        self.playButton.layer.cornerRadius = 5
        self.playButton.setTitle("Play", for: .normal)
        self.playButton.setTitleColor(.white, for: .normal)
        self.playButton.setTitleColor(.red, for: .highlighted)
        self.view.addSubview(playButton)
        self.playButton.addTarget(self, action: #selector(playSound), for: .touchUpInside)
        
        // create stop button parametres
        
        self.stopButton.frame = CGRect(x: 215, y: 500, width: 100, height: 50)
        self.stopButton.backgroundColor = .black
        self.stopButton.layer.cornerRadius = 5
        self.stopButton.setTitle("Stop", for: .normal)
        self.stopButton.setTitleColor(.white, for: .normal)
        self.stopButton.setTitleColor(.red, for: .highlighted)
        self.view.addSubview(stopButton)
        self.stopButton.addTarget(self, action: #selector(stopSound), for: .touchUpInside)
        
        
        
    }
    // MARK: - Functions!!
    
    @objc func sliderFunc(sender: UISlider) {
        if sender == timerForSlider {
            player.currentTime = TimeInterval(timerForSlider.value)
        }

    }

    // time update play the song
    
    @objc func timeUpdate() {
        
        player.volume = musicSlider.value
        timerForSlider.value = Float(player.currentTime)
        let currentTime = Int(player.currentTime)
        let minutes = currentTime / 60
        let second = currentTime - minutes * 60
        let procent = Int(player.volume * 100)
        
        let allTime = Int(player.duration)
        let minutesAll = allTime / 60
        let secondAll = allTime - minutesAll * 60
        
        if procent > 10 {
            volumeTextLabel.text = String(format: "volume: %02d", procent) as String + "%"
        } else {
            volumeTextLabel.text = String(format: "volume: %01d", procent) as String + "%"
        }
        timerTextLabel.text = String(format: "%02d:%02d", minutes, second) as String
        timerTextLabelAll.text = String(format: "%02d:%02d", minutesAll, secondAll) as String
    }
    
    // func play and pause song
    @objc func playSound() {
        if isPlaying {
            player.pause()
            isPlaying = false
            self.playButton.setTitle("Play", for: .normal)
        } else {
            player.play()
            isPlaying = true
            self.playButton.setTitle("Pause", for: .normal)
            
        }
    }
    
    // stop playing the song
    
    @objc func stopSound() {
        player.stop()
        player.currentTime = 0
        isPlaying = false
        self.playButton.setTitle("Play", for: .normal)
    }
    
    
    
    
}



